<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
//use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as MongoModel;
use Illuminate\Support\Facades\DB;
 
class Contacts extends MongoModel 
//class Contactos extends Model
{
    //use HasFactory;
  /*  public static function contacts(){
        return view('contacts');
        //return view('master');
    }
    */
    
	protected $table = 'contacts';
	public $timestamps = false;

	/*protected $casts = [
		'genomi_id' => 'int'
	];*/

	protected $fillable = [
		'name',
		'age',
        'location'
	];

    public static function contacts()
	{
        $contacts = DB::table('contacts')->get();
        return view('contacts', ['contacts' => $contacts]);
            
		/*return Contacts::with('genomi')->with('pruebas_documento')
		->select('pruebas.id', 'pruebas.codigo_sigile', 'pruebas.descripcion', 'genomi.agenda', 'genomi.profesional', 'genomi.codigo', 'pruebas_documentos.mensaje')
		->join('genomi', 'genomi.id', '=', 'pruebas.genomi_id')
		->leftJoin('pruebas_documentos', 'pruebas_documentos.pruebas_id', '=', 'pruebas.id')
		->get();*/
	}
}
